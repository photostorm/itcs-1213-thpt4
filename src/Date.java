/**
 * Date: This class holds a valid date of the Gregorian calendar.
 * 
 * Lab section: L11
 * @author Justin E. Ervin
 * @version 3-27-2014
 */

public class Date {
	// Declaring all needed fields
	// This array holds all of the names of the months
	private String[] monthName = { "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" };

	// This array holds all of the names of the days of the week
	private String[] dayName = { "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday" };
	private int month; // Holds the month of the Date
	private int day; // Holds the day of the Date
	private int year; // Holds the year of a Date

	/**
	 * Default Constructor for objects of class Date
	 */
	public Date() {
		// The default date is Unix Epoch date (January 1, 1970)
		this.month = 1;
		this.day = 1;
		this.year = 1970;
	}

	/**
	 * Copy Constructor for objects of class Date
	 * 
	 * @param other another instance of the Date class
	 */
	public Date(Date other) {
		// Setting the same date from the other Date instance
		this.month = other.month;
		this.day = other.day;
		this.year = other.year;
	}

	/**
	 * Constructor for objects of class Date
	 * 
	 * @param inMonth the month of the Date in number form
	 * @param inDay the day of the Date in number form
	 * @param inYear the year of the Date in number form
	 */
	public Date(int inMonth, int inDay, int inYear) {
		// Checking if the date entered is a valid date
		if (checkDate(inMonth, inDay, inYear)) {
			// If the date is valid, then the date is stored.
			this.month = inMonth;
			this.day = inDay;
			this.year = inYear;
		} else {
			// If the date is not valid, then the date is change to January 1, 1970 (Unix Epoch)
			this.month = 1;
			this.day = 1;
			this.year = 1970;
		}
	}

	/**
	 * add: This method adds the number of days input from user to the current date and finds the
	 * new date with the day added to it. If the number of days inputed are a negative integer
	 * value, then the original date will be returned.
	 * 
	 * @param inDays the number of days to add to the date
	 * @return New Date instance with number of days added to it
	 */
	public Date add(int inDays) {
		// Creating a new Date instance
		Date result = new Date(month, day, year);
		int currentDays = 0; // Holds the number of days that still need to be added
		int currentMonthDays; // Holds the number of days in a partial month

		if (inDays >= 0) {
			// Setting the number of days to add
			currentDays = inDays;

			// Getting the number of days in the current month
			currentMonthDays = getMonthDays(result.month, result.year);

			if (currentMonthDays < (currentDays + result.day)) {
				// Take the number of days to complete the month and subtract it from the number of
				// days that still need to be added
				currentDays = currentDays - (currentMonthDays - result.day);

				// Increase the new month or new year
				if (result.month == 12) {
					result.month = 1;
					result.year = result.year + 1;
				} else {
					result.month = result.month + 1;
				}

				result.day = 0;

				do {
					// Getting the number of days in the current month
					currentMonthDays = getMonthDays(result.month, result.year);

					if (currentMonthDays < currentDays) {
						// If the number of days that still need to be added are greater than the
						// current month`s total days
						currentDays = currentDays - currentMonthDays;

						if (result.month == 12) {
							result.month = 1;
							result.year = result.year + 1;
						} else {
							result.month = result.month + 1;
						}
					} else {
						// Calculate the new day of the date
						result.day = result.day + currentDays;
						currentDays = 0;
					}
				} while (currentDays > 0);
			} else {
				// If the number of days that still need to be added does not go over the current
				// month`s total days, then just add them together
				result.day = result.day + currentDays;
			}
		}
		return result;
	}

	/**
	 * checkDate: This method checks the input date to make sure it is a valid Gregorian calendar
	 * when United States adopt it date
	 * 
	 * @param inMonth the month of the Date in number form
	 * @param inDay the day of the Date in number form
	 * @param inYear the year of the Date in number form
	 * @return Whether or not the date is valid
	 */
	private boolean checkDate(int inMonth, int inDay, int inYear) {
		if (inMonth <= 0 || inDay <= 0 || inYear <= 0) {
			// If the month, day or year is negative, then return false
			return false;
		} else if (inMonth > 12) {
			// If the month has a value greater than 12, then return false;
			return false;
		} else if (inYear < 1752) {
			// If the year is less 1752, then return false because it is not a valid Gregorian date
			// calendar year
			return false;
		} else if (inYear == 1752) {
			if (inMonth < 9) {
				// If the month is less 9 in year of 1752, then return false because it is not a
				// valid Gregorian date
				// calendar year
				return false;
			} else if (inMonth == 9 && inDay < 14) {
				// If the day is less than 14 in the month of September of 1752, then return false
				// because it is not a valid Gregorian date
				// calendar year
				return false;
			}
		} else if (inDay > getMonthDays(inMonth, inYear)) {
			// If the days entered are greater in the inputed month`s total days, then return false
			return false;
		}

		return true;
	}

	/**
	 * compareTo: This method finds the relationship between two dates and return whether or not
	 * this date is later than other date or they are the same.
	 * 
	 * @param other another instance of the Date class
	 * @return The integer value that represent the relationship between two dates (-1 = Other date
	 *         is later, 0 = Equal, 1 = This date is later)
	 */
	public int compareTo(Date other) {
		int result; // Holds the integer value that represent the relationship between two dates

		if (this.year > other.year) {
			// This date instance is later
			result = 1;
		} else if (this.year < other.year) {
			// Other date instance is later
			result = -1;
		} else {
			if (this.month > other.month) {
				// This date instance is later
				result = 1;
			} else if (this.month < other.month) {
				// Other date instance is later
				result = -1;
			} else {
				if (this.day > other.day) {
					// This date instance is later
					result = 1;
				} else if (this.day < other.day) {
					// Other date instance is later
					result = -1;
				} else {
					// Both instances have the same date
					result = 0;
				}
			}
		}

		return result;
	}

	/**
	 * dateDiff: This method calculates the number of days between the two dates. Not including the
	 * end-points
	 * 
	 * @param other another instance of the Date class
	 * @return The number of days between the two dates
	 */
	public int dateDiff(Date other) {
		int result = 0; // Holds the total number of days between the two dates
		int startYear = 0; // Holds the starting year
		int startMonth = 0; // Holds the starting month
		int endMonth = 0; // Holds the ending month
		int endYear = 0; // Holds the ending year

		// Checks if month and year are equal for both this and other date
		// If the month and year are equal, then just find difference in days and return
		if ((this.year == other.year) && (this.month == other.month)) {
			if (other.day > this.day) {
				result = other.day - this.day;
			} else {
				result = this.day - other.day;
			}

			return result;
		}

		// Find the days in the first start and the very last month
		// Advance the start month to the next month, no need to change end month
		if (compareTo(other) > 0) {
			// The date in this instance is after the other date
			result = (getMonthDays(other.month, other.year) - other.day) + this.day;
			
			// Setting the starting point
			startYear = other.year;
			startMonth = other.month + 1;

			// If the starting month is greater than 12, then increase the starting year by one and
			// reset the starting month to January
			if (startMonth > 12) {
				startMonth = 1;
				startYear = startYear + 1;
			}

			// Setting the ending point
			endYear = this.year;
			endMonth = this.month;
		} else if (compareTo(other) < 0) {
			// The date in this instance is before the other date
			result = (getMonthDays(this.month, this.year) - this.day) + other.day;
			
			// Setting the starting point
			startYear = this.year;
			startMonth = this.month + 1;

			// If the starting month is greater than 12, then increase the starting year by one and
			// reset the starting month to January
			if (startMonth > 12) {
				startMonth = 1;
				startYear = startYear + 1;
			}

			// Setting the ending point
			endYear = other.year;
			endMonth = other.month;
		}

		// If the start year and month equal the end year and month then we are done
		if (startYear == endYear && startMonth == endMonth) {
			return result;
		}

		// If the starting year equals the ending year then we just need to count the days in
		// remaining months
		if (startYear == endYear) {
			while (startMonth != endMonth) {
				result = result + (getMonthDays(startMonth, startYear));
				startMonth = startMonth + 1;
			}
			return result;
		}

		// Now we need to find the days in the partial months remain in the start year
		// If starting month equals 1 then we are on a year boundary
		while (startMonth > 1 && startMonth < 13) {
			result = result + (getMonthDays(startMonth, startYear));
			startMonth = startMonth + 1;
		}

		startMonth = 1;
		startYear = startYear + 1;

		// Now we can count the days in the years. This only counts full years.
		while (startYear != endYear) {
			result = result + getYearDays(startYear);
			startYear = startYear + 1;
		}

		// At this point all that remains is the days in the months in the ending year. The days in
		// the very last month have all ready been counted. At this point we are only counting the
		// full remaining months in the final year. The start month at this point should be equal to
		// one, the start of the year startYear should equal endYear at the point
		while (startMonth != endMonth) {
			result = result + (getMonthDays(startMonth, endYear));
			startMonth = startMonth + 1;
		}

		return result;
	}

	/**
	 * equals: This method determines if two dates are equal by compare the month, day and year.
	 * 
	 * @param other another instance of the Date class
	 * @return Whether or not the two dates are equal
	 */
	public boolean equals(Date other) {
		return this.month == other.month && this.day == other.day && this.year == other.year;
	}

	/**
	 * getDateFormat: This method displays this date instance in the partial format that was inputed
	 * 
	 * @param format the reference character for a format (f = Full date format, e = Europe format , l
	 *            = Long text form)
	 * @return The date in it request format
	 */
	public String getDateFormat(char format) {
		String result; // Holds the request date format
		int dayOfWeek = getDayOfWeek(); // Holds the index for the day of the weeks

		if (format == 'f') {
			// The date with the full month name and the day of the week if found.
			if (dayOfWeek >= 0 && dayOfWeek < dayName.length) {
				result = new String(dayName[dayOfWeek] + " " + monthName[month - 1] + " " + day + ", " + year);
			} else {
				result = new String(monthName[month - 1] + " " + day + ", " + year);
			}
		} else if (format == 'e') {
			// The date in the Europe format (DD/MM/YYYY)
			result = new String(day + "/" + month + "/" + year);
		} else if (format == 'l') {
			// The date in long text form
			result = new String(monthName[month - 1] + " the " + day + " of " + year);
		} else {
			// Invalid format request
			result = new String("Invalid Format");
		}

		return result;
	}

	/**
	 * getDayOfWeek: This method calculating the day of the week for this date instance
	 * 
	 * @return The index for the day of the week
	 */
	private int getDayOfWeek() {
		int resultDay = 0; // The current day of the week for the reference point
		int weekDay = 0; // The index for the day of the week
		int daySource = 0; // The day in a partial month which is being used as a reference date

		// Calculating the current day of the week for the reference point
		int difference = year % 100 - 12 * ((year % 100) / 12);
		difference = ((year % 100) / 12) + difference + (difference / 4) + 5 * ((year / 100) % 4) % 7 + 2;

		// Calculating the index for the day of the week for the reference point
		resultDay = difference - 7 * (difference / 7);

		switch (month) {
		case 1:
			if (isLeapYear(year)) {
				// January 4 is the reference date for leap years
				daySource = 4;
			} else {
				// January 3 is the reference date for non-leap years
				daySource = 3;
			}
			break;
		case 2:
		case 3:
			if (isLeapYear(year)) {
				// February 29 is the reference date for leap years
				daySource = 29;
			} else {
				// February 28 is the reference date for non-leap years
				daySource = 28;
			}
			break;
		case 4:
			// April 4 is the reference date
			daySource = 4;
			break;
		case 5:
			// May 9 is the reference date
			daySource = 9;
			break;
		case 6:
			// June 6 is the reference date
			daySource = 6;
			break;
		case 7:
			// July 11 is the reference date
			daySource = 11;
			break;
		case 8:
			// August 8 is the reference date
			daySource = 8;
			break;
		case 9:
			// September 5 is the reference date
			daySource = 5;
			break;
		case 10:
			// October 10 is the reference date
			daySource = 10;
			break;
		case 11:
			// November 7 is the reference date
			daySource = 7;
			break;
		case 12:
			// December 12 is the reference date
			daySource = 12;
			break;
		default:
			// No month found
			daySource = 0;
		}

		if (month == 3) {
			// If the month is March, then , then count forward through the month base on the day of
			// the date
			for (int i = 0; i < day; i++) {
				resultDay++;
				if (resultDay == 7) {
					resultDay = 0;
				}
			}

			// Setting the index for the day of week
			weekDay = resultDay;
		} else if (daySource > day) {
			// If the reference day is greater than the day of the date, then count backward through
			// the difference of the reference day and the day of the date
			for (int i = 0; i < (daySource - day); i++) {
				resultDay--;
				if (resultDay == -1) {
					resultDay = 6;
				}
			}

			// Setting the index for the day of week
			weekDay = resultDay;
		} else if (daySource < day) {
			// If the reference day is less than the day of the date, then count forward through the
			// difference of the day of the date and reference day
			for (int i = 0; i < (day - daySource); i++) {
				resultDay++;
				if (resultDay == 7) {
					resultDay = 0;
				}
			}

			// Setting the index for the day of week
			weekDay = resultDay;
		} else {
			// If the date equals the reference point, then return the index of reference point for
			// the day of week
			weekDay = resultDay;
		}

		return weekDay;
	}

	/**
	 * getDayOfWeekName: This method finds name of the day of the week for this date instance
	 * 
	 * @return The name of the day of the week
	 */
	public String getDayOfWeekName() {
		int dayOfWeek = getDayOfWeek(); // Holds the index for the day of the week

		// Make sure the index for the day of the week is a valid index in the day of week array
		if (dayOfWeek >= 0 && dayOfWeek < dayName.length && checkDate(month, day, year)) {
			return dayName[dayOfWeek];
		} else {
			// If the index for the day of the week is invalid or the date is not valid, then say
			// the day of the week could
			// not be found
			return new String("Unknown");
		}
	}

	/**
	 * getMonthDays: This method checks if the year is a leap year and returns the number of days in
	 * partial month inputed
	 * 
	 * @param inMonth the month of the Date in number form
	 * @param inYear the year of the Date in number form
	 * @return The number of days in partial month
	 */
	private int getMonthDays(int inMonth, int inYear) {
		int result; // Holds the number of days in partial month

		switch (inMonth) {
		case 1:
			// There are 31 days in January
			result = 31;
			break;
		case 2:
			if (isLeapYear(inYear)) {
				// There are 29 days in February for leap years
				result = 29;
			} else {
				// There are 28 days in February for non-leap years
				result = 28;
			}
			break;
		case 3:
			// There are 31 days in March
			result = 31;
			break;
		case 4:
			// There are 30 days in April
			result = 30;
			break;
		case 5:
			// There are 31 days in May
			result = 31;
			break;
		case 6:
			// There are 30 days in June
			result = 30;
			break;
		case 7:
			// There are 31 days in July
			result = 31;
			break;
		case 8:
			// There are 31 days in August
			result = 31;
			break;
		case 9:
			// There are 30 days in September
			result = 30;
			break;
		case 10:
			// There are 31 days in October
			result = 31;
			break;
		case 11:
			// There are 30 days in November
			result = 30;
			break;
		case 12:
			// There are 31 days in December
			result = 31;
			break;
		default:
			// No month found
			result = 0;
		}

		return result;
	}

	/**
	 * getNumberOfLeapYears: This method finds number of leap years between two dates. Not including
	 * the end-points
	 * 
	 * @param other another instance of the Date class
	 * @return The number of leap years between the two dates
	 */
	public int getNumberOfLeapYears(Date other) {
		int result = 0; // Holds the number of leap years between the two dates

		// Finding which year is later to make calculation simple
		if (other.year < this.year) {
			// If the year in this instance is greater than the year in the other instance
			for (int currentYear = other.year; currentYear < this.year; currentYear++) {
				// If the current year equals to one of the end-points, then do not count it
				if (isLeapYear(currentYear) && currentYear != other.year) {
					result += 1;
				}
			}
		} else if (other.year > this.year) {
			// If the year in other instance is greater than the year in the this instance
			for (int currentYear = other.year; currentYear > this.year; currentYear--) {
				// If the current year equals to one of the end-points, then do not count it
				if (isLeapYear(currentYear) && currentYear != other.year) {
					result += 1;
				}
			}
		}

		return result;
	}

	/**
	 * getYearDays: This method checks if the year is a leap year and returns the number of days in
	 * that year
	 * 
	 * @param inYear the full year in number format
	 * @return The number of days in a year
	 */
	private int getYearDays(int inYear) {
		int result; // Holds the number of days in a year

		if (isLeapYear(inYear)) {
			// If the year is a leap year, then return that there are 366 days in that year
			result = 366;
		} else {
			// If the year is not a leap year, then return that there are 365 days in that year
			result = 365;
		}

		return result;
	}

	/**
	 * isLeapYear: This method checks if the year in this instance is a leap year and returns
	 * whether or not the year is a leap year is a leap year
	 * 
	 * @return Whether or not the year is a leap year
	 */
	public boolean isLeapYear() {
		return isLeapYear(year);
	}

	/**
	 * isLeapYear: This method checks if the year is a leap year and returns whether or not the year
	 * is a leap year is a leap year
	 * 
	 * @param inYear the full year in number format
	 * @return Whether or not the year is a leap year
	 */
	private boolean isLeapYear(int inYear) {
		boolean result; // Holds whether or not the year is a leap year

		if (inYear % 4 == 0) {
			if (inYear % 100 == 0) {
				if (inYear % 400 == 0) {
					// The inputed year is a leap year
					result = true;
				} else {
					// The inputed year is not a leap year
					result = false;
				}
			} else {
				// The inputed year is a leap year
				result = true;
			}
		} else {
			// The inputed year is not a leap year
			result = false;
		}

		return result;
	}

	/**
	 * setDate: This method change the current date to a new date
	 * 
	 * @param inMonth the month of the Date in number form
	 * @param inDay the day of the Date in number form
	 * @param inYear the year of the Date in number form
	 */
	public void setDate(int inMonth, int inDay, int inYear) {
		// Checking if the date entered is a valid date
		if (checkDate(inMonth, inDay, inYear)) {
			// If the date is valid, then the date is stored.
			this.month = inMonth;
			this.day = inDay;
			this.year = inYear;
		} else {
			// If the date is not valid, then the date is change to January 1, 1970 (Unix Epoch)
			this.month = 1;
			this.day = 1;
			this.year = 1970;
		}
	}

	/**
	 * subtract: This method subtracts the number of days input from user to the current date and
	 * finds the new date with the day subtracted from original date. If the number of days inputed
	 * are a negative integer value, then the original date will be returned.
	 * 
	 * @param inDays the number of days to subtract from the date
	 * @return New Date instance with number of days subtracted from original date
	 */
	public Date subtract(int inDays) {
		// Creating a new Date instance
		Date result = new Date(month, day, year);
		int currentDays = 0; // Holds the number of days that still need to be subtracted
		int currentMonthDays; // Holds the number of days in a partial month

		if (inDays >= 0) {
			// Setting the number of days to subtract
			currentDays = inDays;

			if (currentDays >= result.day) {
				if (result.month == 1) {
					result.month = 12;
					result.year = result.year - 1;
				} else {
					result.month = result.month - 1;
				}

				// Take the number of days to complete the month and subtract it from the number of
				// days that still need to be subtracted
				currentDays = currentDays - result.day;

				do {
					// Getting the number of days in the current month
					currentMonthDays = getMonthDays(result.month, result.year);

					if (currentMonthDays < currentDays) {
						// If the number of days that still need to be subtracted are greater than
						// the current month`s total days
						currentDays = currentDays - currentMonthDays;

						if (result.month == 1) {
							result.month = 12;
							result.year = result.year - 1;
						} else {
							result.month = result.month - 1;
						}
					} else {
						result.day = currentMonthDays - currentDays;
						if (result.day == 0) {
							// If the day of the date is equal to zero, then set the month or year
							// back and set the day of the date to the last day of the new month
							if (result.month == 1) {
								result.month = 12;
								result.year = result.year - 1;
							} else {
								result.month = result.month - 1;
							}

							result.day = getMonthDays(result.month, result.year);
						}
						currentDays = 0;
					}
				} while (currentDays > 0);
			} else {
				// If the number of days to subtract is less than the current day of the date, then
				// calculate the difference between them for the new day value
				result.day = result.day - currentDays;
			}
		}
		return result;
	}

	/**
	 * toString: This method returns the date in the United State format which is textually
	 * represents this class
	 * 
	 * @return The date in the United State format (MM/DD/YYYY)
	 */
	public String toString() {
		return month + "/" + day + "/" + year;
	}
}
