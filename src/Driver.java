/**
 * Driver: This class was written to test the Date class.
 * 
 * Lab section: L11
 * @author Justin E. Ervin
 * @version 3-27-2014
 */

public class Driver {
	/**
	 * Execution of this program starts in the main( ) method
	 * 
	 * @param Java arguments
	 */
	public static void main(String[] args) {
		// Declares all needed variables
		// Creating a new Date instance with default date (1/1/1970)
		Date myDateOne = new Date();
		// Creating a new Date instance with 4/22/2012
		Date myDateTwo = new Date(4, 22, 2012);
		// Creating a new Date instance with 10/14/2005
		Date myDateThree = new Date(10, 14, 2005);
		
		// Display the information for the first date
		System.out.println("-- First date --");
		System.out.println("My original date is: " + myDateOne);
		
		// Display the first date in different forms
		System.out.println("In long form is: " + myDateOne.getDateFormat('l'));
		System.out.println("In full form is: " + myDateOne.getDateFormat('f'));
		System.out.println("In Europe form is: " + myDateOne.getDateFormat('e'));
		
		// Checking if the first date is in a leap year
		if (myDateOne.isLeapYear()) {
			System.out.println(myDateOne + " is in a leap year");
		} else {
			System.out.println(myDateOne + " is not in a leap year");
		}
		
		// Adding 100 days and subtracting 200 days from the original date of the first date and display the results
		System.out.println("After adding 100 days from original date, the date is: " + myDateOne.add(100) + ". The day of the week is " + myDateOne.add(100).getDayOfWeekName() + ".");
		System.out.println("After subtracting 200 days from original date, the date is: " + myDateOne.subtract(200) + ". The day of the week is " + myDateOne.subtract(200).getDayOfWeekName() + ".");

		// Display the information for the second date
		System.out.println("\n-- Second date --");
		System.out.println("My original date is: " + myDateTwo);
		
		// Display the second date in different forms
		System.out.println("In long form is: " + myDateTwo.getDateFormat('l'));
		System.out.println("In full form is: " + myDateTwo.getDateFormat('f'));
		System.out.println("In Europe form is: " + myDateTwo.getDateFormat('e'));
		
		// Checking if the second date is in a leap year
		if (myDateTwo.isLeapYear()) {
			System.out.println(myDateTwo + " is in a leap year");
		} else {
			System.out.println(myDateTwo + " is not in a leap year");
		}
		
		// Adding 100 days and subtracting 200 days from the original date of the second date and display the results
		System.out.println("After adding 100 days from original date, the date is: " + myDateTwo.add(100) + ". The day of the week is " + myDateTwo.add(100).getDayOfWeekName() + ".");
		System.out.println("After subtracting 200 days from original date, the date is: " + myDateTwo.subtract(200) + ". The day of the week is " + myDateTwo.subtract(200).getDayOfWeekName() + ".");

		// Display the information for the third date
		System.out.println("\n-- Third date --");
		System.out.println("My original date is: " + myDateThree);
		
		// Display the third date in different forms
		System.out.println("In long form is: " + myDateThree.getDateFormat('l'));
		System.out.println("In full form is: " + myDateThree.getDateFormat('f'));
		System.out.println("In Europe form is: " + myDateThree.getDateFormat('e'));
		
		// Checking if the third date is in a leap year
		if (myDateThree.isLeapYear()) {
			System.out.println(myDateThree + " is in a leap year");
		} else {
			System.out.println(myDateThree + " is not in a leap year");
		}
		
		// Adding 100 days and subtracting 200 days from the original date of the third date and display the results
		System.out.println("After adding 100 days from original date, the date is: " + myDateThree.add(100) + ". The day of the week is " + myDateThree.add(100).getDayOfWeekName() + ".");
		System.out.println("After subtracting 200 days from original date, the date is: " + myDateThree.subtract(200) + ". The day of the week is " + myDateThree.subtract(200).getDayOfWeekName() + ".");

		// Display the difference between the first date and second date in days
		System.out.println("\nThe difference between " + myDateOne + " and " + myDateTwo + " is " + myDateOne.dateDiff(myDateTwo) + " days");
		
		// Display the difference between the second date and third date in days
		System.out.println("The difference between " + myDateTwo + " and " + myDateThree + " is " + myDateTwo.dateDiff(myDateThree) + " days");
		
		// Display the number of leap years between the first date and second date
		System.out.println("\nThe number of leap years between " + myDateOne + " and " + myDateTwo + " is " + myDateOne.getNumberOfLeapYears(myDateTwo) + " leap years");
		
		// Checking if the first date is equal to the second date
		if (myDateOne.equals(myDateTwo)) {
			System.out.println("The first date (" + myDateOne + ") and the second date (" + myDateTwo + ") are equal");
		} else {
			System.out.println("The first date (" + myDateOne + ") and the second date (" + myDateTwo + ") are not equal");
		}
		
		// Find the relationship between the first date and second date or if they are the same
		if (myDateOne.compareTo(myDateTwo) > 0) {
			System.out.println("The first date (" + myDateOne + ") is later than the second date (" + myDateTwo + ")");
		} else if (myDateOne.compareTo(myDateTwo) < 0) {
			System.out.println("The second date (" + myDateTwo + ") is later than the first date (" + myDateOne + ")");
		} else {
			System.out.println("The first date (" + myDateOne + ") and the second date (" + myDateTwo + ") are equal");
		}
		
		// Display the information for the new third date
		System.out.println("\n-- New third date --");
		// Changing the third date instance to 2/14/2008
		myDateThree.setDate(2, 14, 2008);
		
		System.out.println("My original date is: " + myDateThree);
		
		// Display the new third date in different forms
		System.out.println("In long form is: " + myDateThree.getDateFormat('l'));
		System.out.println("In full form is: " + myDateThree.getDateFormat('f'));
		System.out.println("In Europe form is: " + myDateThree.getDateFormat('e'));
		
		// Checking if the new third date is in a leap year
		if (myDateThree.isLeapYear()) {
			System.out.println(myDateThree + " is in a leap year");
		} else {
			System.out.println(myDateThree + " is not in a leap year");
		}
		
		// Adding 100 days and subtracting 200 days from the original date of the new third date and display the results
		System.out.println("After adding 100 days from original date, the date is: " + myDateThree.add(100) + ". The day of the week is " + myDateThree.add(100).getDayOfWeekName() + ".");
		System.out.println("After subtracting 200 days from original date, the date is: " + myDateThree.subtract(200) + ". The day of the week is " + myDateThree.subtract(200).getDayOfWeekName() + ".");
	}

}
